`emacs_starter` is a small, basic setup for Emacs to get started with: it sets
up a (subjective) set of better defaults than Vanilla Emacs, and activates some
of the most important features that make Emacs incredible. On the other hand, it
does not go nearly as far as some bundles of Emacs (e.g. Spacemacs). It is
similar in spirit to `better_defaults`, which it includes, but it goes a bit
further.

It is meant as a first stone to step on before embarking on a lifelong adventure
of Emacs tweaking.

# Some of the setup of `emacs_starter`

* Activate `ido`: Ido is an incredible fuzzy matching system when opening files,
  switching buffers, running commands etc. This feature alone speeds up
  productivity significantly!

* Bind C-z to undo, as is standard in almost all other software. This is
  controversial in the Emacs world, but is also one of the things new users find
  appalling. Once you are convinced Emacs is bliss, you may consider removing
  this key binding (though I personally don't see why you should).

  Unfortunately, copy and paste cannot be moved to their standard keys C-c and
  C-v, since especially C-c is used in Emacs as the beginning of hundreds of
  shortcuts.

* Undo-tree is activated by default. Undo-tree is an incredibly powerful undo:
  you've made changes, regret and undo, write something new, and now realise
  that what you wrote first was actually better. In almost all programs, that
  version is now lost, but with Emacs + undo-tree you can get it back. Press
  M-C-z to display a tree of your undo history and browse it.

* Activate the colour-scheme Zenburn.

* More modern text-editing defaults in text-mode and in Org-mode.

* Encourages putting your Emacs setup in a safe, version-controlled place.

* Easier setup on Windows.

# Installation on Linux

TODO


# Installation on Mac

TODO

# Installation on Windows

* Install Emacs from the official download site. Be sure to use the installer
  exe file to install. Run Emacs once and close it again.

* Check out the `emacs_starter` repository.

* Take the file `dot_emacs` in the repo and move to
  `C:/Users/<YourUserName>/AppData/Roaming/emacs`. Rename it to `init.el`

* Open this file. There is a variable `emacs-home` which should be set to the
  full directory name where you want your Emacs setup to be stored. This could
  e.g. be `C:/Users/<YourUserName>/Documents/emacs_setup`. Remember
  quotation-marks around the path, and don't end the path in a `/`.

* Take the contents of the `emacs_setup` folder in the repository and copy into
  the folder you set to `emacs-home` in the last step.

That's it: To check if it works, open Emacs. You shouldn't see the normal
Welcome-screen anymore, and the interface should be colored dark with white
text.

In the future when you wish to make changes your Emacs setup, edit the file
`lisp\init.el` in the folder you set to `emacs-home`.
