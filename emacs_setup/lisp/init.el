;; Specify a folder where all emacs-related stuff should be
;; Most files will be in the .emacs.d sub-folder of emacs-home
(setenv "HOME" emacs-home)
(setq user-emacs-directory (concat emacs-home "/"))
(setq init-home (concat emacs-home "/lisp"))
(add-to-list 'load-path init-home)
;; Setup you do through the menu system will end in the following file
(setq custom-file "~/lisp/emacs-custom.el")
(load custom-file)

;; Setup backup system
(setq backup-by-copying t
      auto-save-file-name-transforms '((".*" "~/.emacs.d/autosaves/\\1" t))  ;; autosaves put away
      backup-directory-alist (quote ((".*" . "~/.emacs.d/backups/"))) ;; backups put away
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t
      )


;; The scratch buffer should be for text
(setq initial-major-mode 'text-mode)
;; New buffers should be text
(setq-default major-mode 'text-mode)

 ; never insert tabs, do 4 spaces
(setq-default indent-tabs-mode nil)
(setq tab-width 4)

;; Always show the current column number by the line number in the status bar
(setq column-number-mode t)

;; Don't show a splash screen or a message
(setq inhibit-splash-screen t)
(setq initial-scratch-message "")

;; Do visual bell instead of stupid sound
(setq visible-bell t)

;; Load the package system
(require 'package)
(setq package-check-signature nil)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))
;; (let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
;;                     (not (gnutls-available-p))))
;;        (url (concat (if no-ssl "http" "https") "://melpa.org/packages/")))
;;   (add-to-list 'package-archives (cons "melpa" url) t))
(package-initialize)

;; Remove annoying and useless shortcuts pressed by mistake
(global-unset-key (kbd "C-x C-c")) ;; for killing emacs fast
(global-unset-key (kbd "C-h h"))   ;; show the HELLO message
(global-unset-key (kbd "C-h C-c")) ;; show the COPYING message
(global-unset-key (kbd "C-h p"))   ;; show the package Finder buffer
(global-unset-key (kbd "C-z"))     ;; minimise Emacs
(global-unset-key (kbd "C-h C-f")) ;; show the FAQ
(global-unset-key (kbd "C-x n d")) ;; narrow to defun
(global-unset-key (kbd "C-x n p")) ;; narrow to page
(global-unset-key (kbd "C-x n n")) ;; narrow to region

;; This activates a number of more sensible defaults for many things in Emacs
(require 'better-defaults)
;; Reinstate the menu bar
(menu-bar-mode 1)
;; Reinstate the scroll bar
(scroll-bar-mode `right)

;; Build and keep list of recent files
(recentf-mode t)
(setq recentf-save-file (concat user-emacs-directory "recentf"))

;; Use the Zenburn visual theme
(setq zenburn-use-variable-pitch t)
(setq zenburn-scale-org-headlines t)
(load-theme 'zenburn t)

;; Set a more common key combination for Undo
(global-set-key (kbd "C-z") 'undo) 
;; Activate the amazing undo-tree
(require 'undo-tree)
(global-undo-tree-mode)
(global-set-key (kbd "C-M-z") 'undo-tree-visualize)

;; Map F3 to open a file name currently under the cursor
(global-set-key [(f4)] 'ffap) 

;; The following code sets special settings for `text-mode`, which is the major
;; mode that will be activated e.g. on ".txt"-files.
(add-hook 'text-mode-hook (lambda ()
  ;; Movement keys move according to the visual layout of lines, revelant when
  ;; Emacs wraps long lines. Then e.g. "up-arrow" moves to the character
  ;; visually above the current one.
  (visual-line-mode)
))

;; The following code sets special settings for `org-mode`, which is the major
;; mode that will be activated on ".org"-files.
(add-hook 'org-mode-hook (lambda ()
  ;; Movement keys move according to the visual layout of lines, revelant when
  ;; Emacs wraps long lines. Then e.g. "up-arrow" moves to the character
  ;; visually above the current one.
  (visual-line-mode)
  ;; Visually indent sub-headings 
  (org-indent-mode)
  ;; Don't automatically insert new-lines at e.g. 80 characters.
  (auto-fill-mode -1)
))


;; Don't show various usual minor modes in status
(require 'diminish)
(cl-loop for minor-mode in '(undo-tree-mode
                             auto-fill-function
                             visual-line-mode
                             highlight-parentheses-mode)
      do (diminish minor-mode))
